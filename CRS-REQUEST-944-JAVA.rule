[

## This rule is also triggered by an Apache Struts exploit:
## [ Apache Struts vulnerability CVE-2017-5638 - Exploit tested: https://github.com/xsscx/cve-2017-5638 ]
##
## This rule is also triggered by an Apache Struts Remote Code Execution exploit:
## [ Apache Struts vulnerability CVE-2017-9791 - Exploit tested: https://www.exploit-db.com/exploits/42324 ]
##
## This rule is also triggered by an Apache Struts Remote Code Execution exploit:
## [ Apache Struts vulnerability CVE-2017-9805 - Exploit tested: https://www.exploit-db.com/exploits/42627 ]
##
## This rule is also triggered by an Oracle WebLogic Remote Command Execution exploit:
## [ Oracle WebLogic vulnerability CVE-2017-10271 - Exploit tested: https://www.exploit-db.com/exploits/43458 ]
    {
        "name": "CRS-JAVA-944100",
        "var": [
            "REQUEST_HEADERS",
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "REQUEST_BODY",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "java\.lang\.(?:runtime|processbuilder)",
        "act": "deny",
        "log": "on",
        "msg": "Remote Command Execution: Suspicious Java class detected. ID:944100"
    },

## This rule is also triggered by the following exploit(s):
## [ Apache Struts vulnerability CVE-2017-5638 - Exploit tested: https://github.com/mazen160/struts-pwn ]
## [ Apache Struts vulnerability CVE-2017-5638 - Exploit tested: https://github.com/xsscx/cve-2017-5638 ]
## [ Apache Struts vulnerability CVE-2017-9791 - Exploit tested: https://www.exploit-db.com/exploits/42324 ]
## [ Apache Struts vulnerability CVE-2017-9805 - Exploit tested: https://www.exploit-db.com/exploits/42627 ]
## [ Oracle WebLogic vulnerability CVE-2017-10271 - Exploit tested: https://www.exploit-db.com/exploits/43458 ]
## [ Apache Struts vulnerability CVE-2018-11776 - Exploit tested: https://www.exploit-db.com/exploits/45262 ]
## [ Apache Struts vulnerability CVE-2018-11776 - Exploit tested: https://www.exploit-db.com/exploits/45260 ]
    {
        "name": "CRS-JAVA-944130",
        "var": [
            "REQUEST_HEADERS",
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "REQUEST_BODY",
            "REQUEST_FILENAME",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": [
            "com\.opensymphony\.xwork2","com\.sun\.org\.apache","java\.io\.BufferedInputStream","java\.io\.BufferedReader","java\.io\.ByteArrayInputStream","java\.io\.ByteArrayOutputStream","java\.io\.CharArrayReader","java\.io\.DataInputStream","java\.io\.File","java\.io\.FileOutputStream","java\.io\.FilePermission","java\.io\.FileWriter","java\.io\.FilterInputStream","java\.io\.FilterOutputStream","java\.io\.FilterReader","java\.io\.InputStream","java\.io\.InputStreamReader","java\.io\.LineNumberReader","java\.io\.ObjectOutputStream","java\.io\.OutputStream","java\.io\.PipedOutputStream","java\.io\.PipedReader","java\.io\.PrintStream","java\.io\.PushbackInputStream","java\.io\.Reader","java\.io\.StringReader","java\.lang\.Class","java\.lang\.Integer","java\.lang\.Number","java\.lang\.Object","java\.lang\.Process","java\.lang\.ProcessBuilder","java\.lang\.reflect","java\.lang\.Runtime","java\.lang\.String","java\.lang\.StringBuilder","java\.lang\.System","javax\.script\.ScriptEngineManager","org\.apache\.commons","org\.apache\.struts","org\.apache\.struts2","org\.omg\.CORBA","java\.beans\.XMLDecode"
        ],
        "act": "deny",
        "log": "on",
        "msg": "Suspicious Java class detected. ID:944130"
    },

## [ Java deserialization vulnerability/Apache Commons (CVE-2015-4852) ]
##
## Detect exploitation of "Java deserialization" Apache Commons.
    {
        "name": "CRS-JAVA-944200",
        "var": [
            "REQUEST_HEADERS",
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "REQUEST_BODY",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "\xac\xed\x00\x05",
        "act": "deny",
        "log": "on",
        "msg": "Magic bytes Detected, probable java serialization in use. ID:944200"
    },

## Detecting possible base64 text to match encoded magic bytes \xac\xed\x00\x05 with padding encoded in base64 strings are rO0ABQ KztAAU Cs7QAF
    {
        "name": "CRS-JAVA-944210",
        "var": [
            "REQUEST_HEADERS",
            "REQUEST_BODY",
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?:rO0ABQ|KztAAU|Cs7QAF)",
        "act": "deny",
        "log": "on",
        "msg": "Magic bytes Detected Base64 Encoded, probable java serialization in use. ID:944210"
    },

## 
    {
        "name": "CRS-JAVA-944240",
        "var": [
            "REQUEST_HEADERS",
            "REQUEST_BODY",
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?:clonetransformer|forclosure|instantiatefactory|instantiatetransformer|invokertransformer|prototypeclonefactory|prototypeserializationfactory|whileclosure|getproperty|filewriter|xmldecoder)",
        "act": "deny",
        "log": "on",
        "msg": "Remote Command Execution: Java serialization (CVE-2015-4852). ID:944240"
    },

## This rule is also triggered by the following exploit(s):
## [ SAP CRM Java vulnerability CVE-2018-2380 - Exploit tested: https://www.exploit-db.com/exploits/44292 ]
    {
        "name": "CRS-JAVA-944250",
        "var": [
            "REQUEST_HEADERS",
            "REQUEST_BODY",
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "java\b.+(?:runtime|processbuilder)",
        "act": "deny",
        "log": "on",
        "msg": "Remote Command Execution: Suspicious Java method detected. ID:944250"
    },

## Interesting keywords for possibly RCE on vulnerable classes and methods base64 encoded
## Keywords = ['runtime', 'processbuilder', 'clonetransformer', 'forclosure', 'instantiatefactory', 'instantiatetransformer', 'invokertransformer', 'prototypeclonefactory', 'prototypeserializationfactory', 'whileclosure']
    {
        "name": "CRS-JAVA-944300",
        "var": [
            "REQUEST_HEADERS",
            "REQUEST_BODY",
            "REQUEST_COOKIES",
            "REQUEST_COOKIES_NAMES",
            "ARGS_GET_NAMES",
            "ARGS_GET",
            "ARGS_POST_NAMES",
            "ARGS_POST"
        ],
        "op": "re",
        "value": "(?:cnVudGltZQ|HJ1bnRpbWU|BydW50aW1l|cHJvY2Vzc2J1aWxkZXI|HByb2Nlc3NidWlsZGVy|Bwcm9jZXNzYnVpbGRlcg|Y2xvbmV0cmFuc2Zvcm1lcg|GNsb25ldHJhbnNmb3JtZXI|BjbG9uZXRyYW5zZm9ybWVy|Zm9yY2xvc3VyZQ|GZvcmNsb3N1cmU|Bmb3JjbG9zdXJl|aW5zdGFudGlhdGVmYWN0b3J5|Gluc3RhbnRpYXRlZmFjdG9yeQ|BpbnN0YW50aWF0ZWZhY3Rvcnk|aW5zdGFudGlhdGV0cmFuc2Zvcm1lcg|Gluc3RhbnRpYXRldHJhbnNmb3JtZXI|BpbnN0YW50aWF0ZXRyYW5zZm9ybWVy|aW52b2tlcnRyYW5zZm9ybWVy|Gludm9rZXJ0cmFuc2Zvcm1lcg|BpbnZva2VydHJhbnNmb3JtZXI|cHJvdG90eXBlY2xvbmVmYWN0b3J5|HByb3RvdHlwZWNsb25lZmFjdG9yeQ|Bwcm90b3R5cGVjbG9uZWZhY3Rvcnk|cHJvdG90eXBlc2VyaWFsaXphdGlvbmZhY3Rvcnk|HByb3RvdHlwZXNlcmlhbGl6YXRpb25mYWN0b3J5|Bwcm90b3R5cGVzZXJpYWxpemF0aW9uZmFjdG9yeQ|d2hpbGVjbG9zdXJl|HdoaWxlY2xvc3VyZQ|B3aGlsZWNsb3N1cmU)",
        "act": "deny",
        "log": "on",
        "msg": "Base64 encoded string matched suspicious keyword. ID:944300"
    }

]